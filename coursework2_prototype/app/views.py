from flask import render_template, flash, redirect, session, request, url_for, make_response, request
from app import app, db, models
from .forms import UserDetails, LogOut, Request
from datetime import datetime, date, timedelta
import qrcode

def testNewUser(form): # tests correct changes are made to DB when ussr logs in
    passed = True
    if models.Logs.query.order_by(models.Logs.id.desc()).first().name != form.name.data:
        passed = False #check correct name
    if models.Logs.query.order_by(models.Logs.id.desc()).first().phone != form.telNumber.data:
        passed = False #check correct phone
    if models.Logs.query.order_by(models.Logs.id.desc()).first().email != form.email.data:
        passed = False #check correct email
    if models.Logs.query.order_by(models.Logs.id.desc()).first().address != form.address.data:
        passed = False #check correct address
    if models.Logs.query.order_by(models.Logs.id.desc()).first().checkIn.date() != datetime.now().date():
        passed = False #check correct date for check in

    if passed == True:
        print("PASSED: User Added Successfully") #all fields correct
    else:
        print("FAILED: Error occured adding user") 
    return

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = UserDetails()
    print(request.cookies.get('user_id'))
    if request.cookies.get('user_id'):  #if the user has cookie form previous log in
        u_name = models.Accounts.query.filter_by(id=request.cookies.get('user_id')).first().name
        u_phone = models.Accounts.query.filter_by(id=request.cookies.get('user_id')).first().phone
        u_email = models.Accounts.query.filter_by(id=request.cookies.get('user_id')).first().email
        u_address = models.Accounts.query.filter_by(id=request.cookies.get('user_id')).first().address
        log = models.Logs(name=u_name, phone=u_phone, email=u_email, address=u_address, checkIn=datetime.now())
        db.session.add(log)
        db.session.commit()
        session['id'] = models.Logs.query.filter_by(name=u_name, phone=u_phone, email=u_email, address=u_address).order_by(models.Logs.id.desc()).first().id
        return redirect("/checkout")

    if form.validate_on_submit():#new user logs in
        log = models.Logs(name=form.name.data, phone=form.telNumber.data, email=form.email.data, address=form.address.data, checkIn=datetime.now())
        db.session.add(log)

        if form.remember.data==True: #send user cookie if they have chosen to be remembered for future log ins
            ac = models.Accounts(name=form.name.data, phone=form.telNumber.data, email=form.email.data, address=form.address.data)
            response = make_response(redirect("/checkout"))
            db.session.add(ac)
            db.session.commit()
            response.set_cookie('user_id', str(models.Accounts.query.filter_by(name=form.name.data, phone=form.telNumber.data, email=form.email.data, address=form.address.data).order_by(models.Accounts.id.desc()).first().id))
            session['id'] = models.Logs.query.filter_by(name=form.name.data, phone=form.telNumber.data, email=form.email.data, address=form.address.data).order_by(models.Logs.id.desc()).first().id

            return response 
        db.session.commit()
        testNewUser(form)
        session['id'] = models.Logs.query.filter_by(name=form.name.data, phone=form.telNumber.data, email=form.email.data, address=form.address.data).order_by(models.Logs.id.desc()).first().id
        return redirect("/checkout")
    return render_template('register.html',
                           title='Register',
                           form=form)

@app.route('/checkout', methods=['GET', 'POST'])
def complete(): # sign users out
    form = LogOut()
    form.id = session['id']
    print("ID:  " +str(form.id))
    if form.is_submitted():
        log = models.Logs.query.get(form.id)
        log.checkOut = datetime.now()
        db.session.commit()
        return redirect("/request_contacts")
    return render_template('checkout.html', title='Check Out', form=form, name=models.Logs.query.get(form.id).name)

@app.route('/request_contacts', methods=['GET', 'POST'])
def contacts(): #allows user to retrieve table of user check ins
    form = Request()
    count = models.Logs.query.filter_by(checkOut = None).count()

    if form.is_submitted():
        logs = models.Logs.query.filter(models.Logs.checkIn >form.date.data).filter(models.Logs.checkIn <form.date.data+timedelta(days=1)).all()
        return render_template('requested_contacts.html', title='Request Contacts', form=form, logs=logs, count=count)
    return render_template('request_contacts.html', title='Request Contacts', form=form, count = count)

@app.route('/create_qr')
def createQR(): # produces qr code linking to register page

    qr = qrcode.QRCode(version=1, box_size=10, border=5)
    qr.add_data(request.url_root + "register")
    qr.make(fit = True)

    image = qr.make_image(fill='black', back_colour='white')
    image.save('app/static/qrcode.png')
    return render_template('qr.html')


