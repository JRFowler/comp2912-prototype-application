from app import db
from datetime import date, datetime
class Logs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    phone = db.Column(db.String(500))
    email = db.Column(db.String(500))
    address = db.Column(db.String(500))
    checkIn = db.Column(db.DateTime)
    checkOut = db.Column(db.DateTime)

class Accounts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    phone = db.Column(db.String(500))
    email = db.Column(db.String(500))
    address = db.Column(db.String(500))
