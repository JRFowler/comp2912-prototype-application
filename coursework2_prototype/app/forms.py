from flask_wtf import Form
from wtforms import StringField, BooleanField, IntegerField, DateField
from wtforms.validators import DataRequired

class UserDetails(Form):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email')
    telNumber = StringField('Phone number')
    address = StringField('Address')
    remember = BooleanField('Save details for next time')

class LogOut(Form):
    id = IntegerField('id')
    
class Request(Form):
    date = DateField('Date', format='%d/%m/%Y',  validators=[DataRequired()])
