"""second migration

Revision ID: 14613e725763
Revises: aac86e4b67ef
Create Date: 2020-11-18 16:29:45.588086

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '14613e725763'
down_revision = 'aac86e4b67ef'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('accounts', sa.Column('name', sa.String(length=500), nullable=True))
    op.add_column('logs', sa.Column('name', sa.String(length=500), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('logs', 'name')
    op.drop_column('accounts', 'name')
    # ### end Alembic commands ###
